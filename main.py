l = ['a', 'b', 'c', 'd', 'e', 'f']

def binary_search(array_to_sort, what_search):
    """Для бинарного поиска, список до этого должен быть отсортирован"""
    first_pos = 0
    last_pos = len(array_to_sort) - 1 # Переменная для определения длины массива

    while first_pos <= last_pos:
        center_value = (first_pos + last_pos) // 2 # Для определения середины массива, мы делим первую позицию и последнюю на два
        # //-это знак деление, чтобы нам пришло только целое число
        if array_to_sort[center_value] == what_search:
            """Это на случай, если с первого вхождения найдем значение"""
            return array_to_sort.index(array_to_sort[center_value])
        else:
            if what_search < array_to_sort[center_value]:
                # Сравнение букв по алфавиту
                last_pos = center_value - 1 # Смещаемся налево и проверяем его значение
            else:
                first_pos = center_value + 1 # Смещаемся направо и проверяем его значение
    return False

print(binary_search(l, 'w'))